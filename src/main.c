#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"

void print_n_test(char* const description) {
    static size_t n_test = 1;
    printf("--- Test № %zu ---\n%s\n", n_test, description);
    n_test++;
}

int main() {
    void* heap = heap_init(18081991);
    debug_heap(stdout, heap);

    print_n_test("Normal Allocation");
    void* block1 = _malloc(1000);
    void* block2 = _malloc(4000);
    _malloc(1000);
    void* block3 = _malloc(1);
    _malloc(10000);
    debug_heap(stdout, heap);


    print_n_test("Free last block");
    _free(block3);
    debug_heap(stdout, heap);

    print_n_test("Free two blocks");
    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);

    print_n_test("The memory is over, the new region of memory expands the old");
    _malloc(2000);
    _malloc(10000000);
    _malloc(10300000);
    _malloc(6666);
    debug_heap(stdout, heap);

    return 0;
}

